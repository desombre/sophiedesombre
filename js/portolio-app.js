var portfolioApp = angular.module('portfolioApp', []);

portfolioApp.controller('PortfolioController', function PortfolioController($scope) {
    $scope.whoAmI = {
        title: "Wer bin ich überhaupt?",
        text: "Momentan bin ich Studentin der Betriebswirtschaft an der Frankfurt University of " +
            "Applied Sciences in Frankfurt am Main. Ich bin eine kontaktfreudige, organisierte und " +
            "engagierte Person.",
        pictures: [
            {src: "./img/bg-img/Reisebild3.JPG", alt: "Blumen!"},
            {src: "./img/bg-img/Reisebild2.JPG", alt: "Paris!"}
        ]
    };

    $scope.education = [
        { text: " 09.2008 - 06.2016 <b>Gymnasium</b> Friedrich-Wöhler-Gymnasium, Singen" },
        { text: "06.2016 <b> Abschluss</b> Abitur 2016 (mit Abschlussnote 1,8)" },
        { text: " Seit 10.2018 <b> Frankfurt University of Applied Sciences</b> Studiengang: Betriebswirtschaft B. A. mit Schwerpunkt Controlling und betriebliche Steuerlehre, 7. Semester, momentaner Schnitt: 1,6" },
        { text: "01.2019 – 04.2019 <b>Auslandssemester</b> an der Université Catholique de Lyon, Frankreich" }
    ]

    $scope.jobs = [
        { text: "09.2013 - 07.2016 <b>Ballettlehrerin</b> der Vorstufe, Ballettschule Färbe, Singen" },
        { text: " 08.2016 - 11.2016 <b>Kinder- und Sportanimation</b>, TUI Service AG, Hotel Maryland auf Formentera und Hotel Vell Mari auf Mallorca" },
        { text: " 06.2017 - 09.2017 <b>Kinder- und Teensanimation</b>, TUI Service AG,        Hotel Nana Beach auf Kreta" },
        { text: "09.2018 – 03.2020 Mitarbeiterin <b>Verkauf</b> bei Immergrün AG" },
        { text: "Seit 11.2020 Tutorin <b>Internes Rechnungswesen</b> an der Frankfurt University of Applied Sciences" }

    ]

    $scope.practicum = [
        { text: "03.2017 Praktikum in der <b>Schulsozialarbeit</b>, Ten-Brink-Schule, Rielasingen" },
        { text: "04.2020 – 09.2020 Praxissemester: <b>Praktikum bei tegut… gute Lebensmittel GmbH & Co.</b>        KG in der Abteilung <b>Controlling</b>" },

    ]

    $scope.projects = [
        { text: "07.2007 - 07.2016 <b>Ministrantin & Gruppenleiterin</b>, Seelsorgeeinheit Aachtal" },
        { text: "09.2013 – 07.2016 <b>Klassen- und Kurssprecherin</b>, Friedrich-Wöhler-Gymnasium" },
        { text: "09.2019 – 03.2020 <b> Betreuende für ausländische Studierende</b> der Frankfurt University of Applied Sciences" },
        { text: "  Seit 03.2020 <b>Mitglied im Prüfungsausschuss</b> an der Frankfurt University of Applied Sciences" },

    ]

    $scope.whatIHaveDone = [
        { icon: "fa-graduation-cap", title: "Bildung", content: $scope.education },
        { icon: "fa-briefcase", title: "Jobs", content: $scope.jobs },
        { icon: "fa-id-badge", title: "Praktika", content: $scope.practicum },
        { icon: "fa-heart", title: "Projekte", content: $scope.projects }

    ]

    $scope.allICanDo = {
        title: "Was kann ich alles?",
        text: "An der Uni liebe ich, dass einem so viele Möglichkeiten geboten werden, " +
            "Schwerpunkte und Vertiefungen frei zu wählen. Eine gute Organisation und " +
            "strukturierte Herangehensweise sind zwei meiner wesentlichen Stärken. Privat " +
            "beschäftige ich mich ausführlich mit dem Thema Veganismus und Nachhaltigkeit. "
    }

    $scope.hobbies = [
        { icon: "fa-utensils", title: "Kochen", text: "Ich liebe es vegetarische und vegane Rezepte auszuprobieren, so wie nach Gefühl zu kochen. Immer unter dem Motto:<a href='https://www.instagram.com/zofi.food/'> Das Auge isst mit. </a>" },
        { icon: "fa-shoe-prints", title: "Sport", text: "Am liebsten spiele ich Tischtennis, gehe Joggen oder mache Yoga. Außerdem mache ich Kickbox-Aerobic an der Frankfurt University of Applied Sciences." },
        { icon: "fa-plane-departure", title: "Reisen", text: "Länger an dem gleichen Ort zu sein, fällt mir schwer. Ich brauche immer Abwechslung und will soviel entdecken wie möglich. Es ist ein Privileg, dass ich sowohl die Zeit als auch die Mittel dazu habe, die ganze Welt zu entdecken. Meine Empfehlungen: Formentera, Lyon, Bodensee, Lissabon, Kapstadt, Bali uvm." }

    ]

    $scope.contact = {
        email: "sophiedesombre@hotmail.com", address: "Talwiesenstrasse 21 // 78239 Rielasingen-Arlen"
    }


});

portfolioApp.filter('asHtml', ['$sce', function ($sce) {
    return function (text) {
        return $sce.trustAsHtml(text);
    };
}]);

portfolioApp.filter('reverse', function() {
    return function(items) {
      return items.slice().reverse();
    };
  });